#!/usr/bin/env python3

# Part 1 - tasks from slide 50

class Dog:
    def __init__(self, dog_name: str = "Dog", dog_legs: int = 4):
        print("Created a new dog")
        self.name = dog_name
        self.legs = dog_legs


class Shihtzu(Dog):
    def __init__(self):
        super(Shihtzu, self).__init__()
        print("Created new shihtzu")


print("="*60)
shihtzu = Shihtzu()
print(shihtzu.name)
print(str(shihtzu.legs))


class Cat:
    def __init__(self):
        print("Created a new cat")

    def introduce(self) -> None:
        print("I'm a cat, but which one?")


class Sphinx(Cat):
    def __init__(self):
        print("Created a new sphinx")


print("="*60)
sphinx = Sphinx()
sphinx.introduce()


class Scale:
    def __init__(self, scale_name: str = "Scale", tones: int = 0):
        print("Created new scale")
        self.name = scale_name
        self.tones = tones

    def plays(self) -> None:
        print("I don't know which sounds I can play")


class Cdur(Scale):
    def __init__(self):
        super(Cdur, self).__init__("Cdur", 7)
        print("Creating Cdur scale")

    def plays(self) -> None:
        print("I can play sounds: c, d, e, f, g, a, b")


print("="*60)
scale = Scale()
scale.plays()
print(scale.name)
print(scale.tones)

print("="*60)
cdur = Cdur()
cdur.plays()
print(cdur.name)
print(cdur.tones)

# Part 2 - tasks from slide 56

print("="*60)
first_name: str = input("What is your name? ")
last_name: str = input("What is your surname? ")
age: int = input("How old are you? ")

print("="*60)
print("Hello " + first_name + " " + last_name + ". You are " + str(age) + " years old.")

print("="*60)
number: str = input("Insert a number: ")


def change_type(number: str) -> None:
    print("Number before changing type is instance: {}".format(type(number).__name__))
    number: int = int(number)
    print("Number after changing type is instance: {}".format(type(number).__name__))


change_type(number)


class Flower:
    def __init__(self, name: str, color: str):
        self.name = name
        self.color = color

    def describe_flower(self):
        print("Your flower is {}. It has {} color.".format(self.name, self.color))


print("="*60)
flower_name: str = input("Input flower name: ")
flower_color: str = input("Input flower color: ")
your_flower = Flower(flower_name,flower_color)
your_flower.describe_flower()

# Part 3 - tasks from slide 71


def check_name(name: str) -> None:
    if name == "Dawid":
        print("Hello my namesake!")
    else:
        print("Hello {}!".format(name))


class Circle:
    def __init__(self, circ: int, radius: int, name: str):
        self.circ: int = circ
        self.radius: int = radius
        self.name: int = name

    def __eq__(self, other):
        print("Comparison type: {} == {}".format(self.name, other.name))
        return self.circ == other.circ and self.radius == other.radius

    def __lt__(self, other):
        print("Comparison type: {} < {}".format(self.name, other.name))
        return self.circ < other.circ and self.radius < other.radius

    def __gt__(self, other):
        print("Comparison type: {} > {}".format(self.name, other.name))
        return self.circ > other.circ and self.radius > other.radius

    def __le__(self, other):
        print("Comparison type: {} <= {}".format(self.name, other.name))
        return self.circ <= other.circ and self.radius <= other.radius

    def __ge__(self, other):
        print("Comparison type: {} >= {}".format(self.name, other.name))
        return self.circ >= other.circ and self.radius >= other.radius

    def __ne__(self, other):
        print("Comparison type: {} != {}".format(self.name, other.name))
        return self.circ != other.circ and self.radius != other.radius


a = Circle(24, 4, "A")
b = Circle(30, 5, "B")

print("="*60)
print("1. A == B = {}".format(a == b))
print("2. A < B = {}".format(a < b))
print("3. A > B = {}".format(a > b))
print("4. A <= B = {}".format(a <= b))
print("5. A >= B = {}".format(a >= b))
print("6. A != B = {}".format(a != b))


class Cube:
    def __init__(self, side: int, name: str):
        self.side: int = side
        self.name: int = name
        self.volume: int = pow(side, 3)

    def __eq__(self, other):
        print("Comparison type: {} == {}".format(self.name, other.name))
        return self.volume == other.volume

    def __lt__(self, other):
        print("Comparison type: {} < {}".format(self.name, other.name))
        return self.volume < other.volume

    def __gt__(self, other):
        print("Comparison type: {} > {}".format(self.name, other.name))
        return self.volume > other.volume

    def __le__(self, other):
        print("Comparison type: {} <= {}".format(self.name, other.name))
        return self.volume <= other.volume

    def __ge__(self, other):
        print("Comparison type: {} >= {}".format(self.name, other.name))
        return self.volume >= other.volume

    def __ne__(self, other):
        print("Comparison type: {} != {}".format(self.name, other.name))
        return self.volume != other.volume


c = Cube(6, "C")
d = Cube(5, "D")

print("="*60)
print("1. C == D = {}".format(c == d))
print("2. C < D = {}".format(c < d))
print("3. C > D = {}".format(c > d))
print("4. C <= D = {}".format(c <= d))
print("5. C >= D = {}".format(c >= d))
print("6. C != D = {}".format(c != d))


class ChessTitle:
    def __init__(self, name: str  = "None", rank: int = 0):
        print("Created new title.")
        self.name = name
        self.rank = rank

    def introduce(self) -> None:
        print("I don't know who I am...")

    def __eq__(self, other):
        print("Comparison type: {} == {}".format(self.name, other.name))
        return self.rank == other.rank

    def __lt__(self, other):
        print("Comparison type: {} < {}".format(self.name, other.name))
        return self.rank < other.rank

    def __gt__(self, other):
        print("Comparison type: {} > {}".format(self.name, other.name))
        return self.rank > other.rank

    def __le__(self, other):
        print("Comparison type: {} <= {}".format(self.name, other.name))
        return self.rank <= other.rank

    def __ge__(self, other):
        print("Comparison type: {} >= {}".format(self.name, other.name))
        return self.rank >= other.rank

    def __ne__(self, other):
        print("Comparison type: {} != {}".format(self.name, other.name))
        return self.rank != other.rank


class GrandMaster(ChessTitle):
    def __init__(self):
        super(GrandMaster, self).__init__("GM", 2500)
        print("Created Grandmaster title.")

    def introduce(self) -> None:
        print("I'm a grandmaster. I have 2500 ranking.")


class InternationalMaster(ChessTitle):
    def __init__(self):
        super(InternationalMaster, self).__init__("IM", 2400)
        print("Created International Master title.")

    def introduce(self) -> None:
        print("I'm a international master. I have 2400 ranking.")


class FideMaster(ChessTitle):
    def __init__(self):
        super(FideMaster, self).__init__("FM", 2300)
        print("Created FIDE master title.")

    def introduce(self) -> None:
        print("I'm a FIDE master. I have 2300 ranking.")


class NationalMaster(ChessTitle):
    def __init__(self):
        super(NationalMaster, self).__init__("NM", 2200)
        print("Created national master title.")

    def introduce(self) -> None:
        print("I'm a national master. I have 2200 ranking.")


class CandidateMaster(ChessTitle):
    def __init__(self):
        super(CandidateMaster, self).__init__("NM", 2200)
        print("Created national master title.")

    def introduce(self) -> None:
        print("I'm a national master. I have 2100 ranking.")


print("="*60)
grandmaster = GrandMaster()
grandmaster.introduce()
internationalmaster = InternationalMaster()
internationalmaster.introduce()
fidemaster = FideMaster()
fidemaster.introduce()
nationalmaster = NationalMaster()
nationalmaster.introduce()
candidatemaster = CandidateMaster()
candidatemaster.introduce()


print("1. grandmaster == internationalmaster = {}".format(grandmaster == internationalmaster))
print("2. fidemaster < candidatemaster = {}".format(fidemaster < candidatemaster))
print("3. grandmaster > nationalmaster = {}".format(grandmaster > nationalmaster))
print("4. nationalmaster <= candidatemaster = {}".format(nationalmaster <= candidatemaster))
print("5. internationalmaster >= fidemaster = {}".format(internationalmaster >= fidemaster))
print("6. candidatemaster != grandmaster = {}".format(candidatemaster != grandmaster))